# Pizza.py toolkit, www.cs.sandia.gov/~sjplimp/pizza.html
# Steve Plimpton, sjplimp@sandia.gov, Sandia National Laboratories
#
# Copyright (2005) Sandia Corporation.  Under the terms of Contract
# DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
# certain rights in this software.  This software is distributed under 
# the GNU General Public License.

# POSCAR tool

oneline = "Convert LAMMPS snapshots to POSCAR format"

docstr = """
x = POSCAR(d)		d = object containing atom coords (dump, data)

x.one()                 write first snapshots to POSCAR
x.single(N)             write snapshot for timestep N to POSCAR 
x.single(N,"file")      write snapshot for timestep N to file.vasp
"""

# History
#   8/05, Steve Plimpton (SNL): original version

# ToDo list

# Variables
#   data = data file to read from

# Imports and external programs

import sys

# Class definition

class POSCAR:

  # --------------------------------------------------------------------

  def __init__(self,data):
    self.data = data
   
  # --------------------------------------------------------------------

  def one(self):
    self.single(0)

      
  
  
  # --------------------------------------------------------------------

  def single(self,time,*args):
    if len(args) == 0: file = "POSCAR"
    elif args[0][-5:] == ".vasp": file = args[0]
    else: file = args[0] + ".vasp"
    #self.data.scale()
    which = self.data.findtime(time)
    time,box,atoms,bonds,tris,lines = self.data.viz(which)
    f = open(file,"w")
    print >>f,self.data.title,
    print >>f,1.0 #lattice constant
    print >>f,"%f\t0.0\t0.0" % (float(box[3])-float(box[0]))
    print >>f,"0.0\t%f\t0.0" % (float(box[4])-float(box[1]))
    print >>f,"0.0\t0.0\t%f" % (float(box[5])-float(box[2]))
    print >>f,"Cartesian"
    #print >>f,len(atoms)
    typesatom=[atom[1] for atom in atoms ]
    types=list(set(typesatom))
    ntype=len(types)
    atomOfType={}
    for type in types:
        atomOfType[type]=[atom for atom in atoms if atom[1]==type]
    for type in types:
        print >>f,"%d\t" % (len(atomOfType[type])),
    print >>f,""
    for type in types:
	  for atom in atomOfType[type]:
		  itype = int(atom[1])
		  print >>f,itype,atom[2],atom[3],atom[4]

    f.close()
